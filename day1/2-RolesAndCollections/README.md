# Exercise 2 - Exploring Roles and Collections

## Table of Contents

- [Exercise 2 - Exploring Roles and Collections](#exercise-2---exploring-roles-and-collections)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Diagram](#diagram)
  - [Guide](#guide)
    - [Step 1 - Examining Execution Environments](#step-1---examining-execution-environments)
    - [Step 2 - Installing a collection](#step-2---installing-a-collection)
    - [Using Collection Content in a Playbook](#using-collection-content-in-a-playbook)
    - [Using a Role in a Playbook](#using-a-role-in-a-playbook)
  - [Complete](#complete)

## Objective

These first few lab exercises will explore Ansible automation components such as Roles and Collections, and the command-line utilities of the Ansible Automation Platform.  This includes:

***Automation Components***

- [Ansible roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html) - Ansible Roles let you automatically load related vars, files, tasks, handlers, and other Ansible artifacts based on a known file structure. After you group your content in roles, you can easily reuse them and share them with other users.  Think about this as automation that is prepackaged for use - you just need to add variables and run it.
- [Ansible Collections](https://docs.ansible.com/ansible/latest/collections_guide/index.html) - Ansible Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins. 

***Automation Utilities***
- [ansible-core](https://docs.ansible.com/core.html) - the base executable that provides the framework, language and functions that underpin the Ansible Automation Platform.  It also includes various cli tools like `ansible`, `ansible-playbook` and `ansible-doc`.  Ansible Core acts as the bridge between the upstream community with the free and open source Ansible and connects it to the downstream enterprise automation offering from Red Hat, the Ansible Automation Platform.
- [ansible-navigator](https://github.com/ansible/ansible-navigator) - a command line utility and text-based user interface (TUI) for running and developing Ansible automation content.
- [Execution Environments](https://docs.ansible.com/automation-controller/latest/html/userguide/execution_environments.html) - not specifically covered in this workshop because the built-in Ansible Execution Environments already included all the Red Hat supported collections which includes all the network collections we use for this workshop.  Execution Environments are container images that can be utilized as Ansible execution.
- [Ansible Collections](https://docs.ansible.com/ansible/latest/collections_guide/index.html) - Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins. You can install and use collections through a distribution server, such as Ansible Galaxy, or a Pulp 3 Galaxy server such as Ansible Automation Platform Private Automation Hub.  Collections are the primary means of sharing automation content in Ansible (sharing content such as a Role outside of a collection is not supported in Private Automation Hub).


## Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/raw/devel/images/ansible_network_diagram.png)



## Guide

### Step 1 - Examining Execution Environments

Run the `ansible-navigator` command with the `:images` argument to look at execution environments configured on the control node:

```bash
$ ansible-navigator
```

![ansible-navigator images](images/ansible-navigator-init.png)

```
Enter `:images` to see the images available
```

![ansible-navigator images](images/navigator-images.png)

This command gives you information about all currently installed Execution Environments or EEs for short.  Investigate an EE by pressing the corresponding number.  For example pressing `0` with the above example will open the `network-ee` execution environment:

![ee main menu](images/navigator-ee-menu.png)

Selecting `2` for `Ansible version and collections` will show us all Ansible Collections installed<b> `on that particular EE`</b>, and the version of `ansible-core`.  Collections are a distribution format for Ansible content that can include playbooks, roles, modules, and plugins. 


![ee info](images/navigator-ee-collections.png)

When referring to a non-built in module, there are three important fields:

```
namespace.collection.module
```
For example:
```
cisco.ios.facts
```

Explanation of terms:
- **namespace** - example **cisco** - A namespace is grouping of multiple collections.  The **cisco** namespace contains multiple collections including **ios**, **nxos**, and **iosxr**.
- **collection** - example **ios** - A collection is a distribution format for Ansible content that can include playbooks, roles, modules, and plugins.  The **ios** collection contains all the modules for Cisco IOS/IOS-XE
- **module** - example facts - Modules are discrete units of code that can be used in a playbook task. For example the **facts** modules will return structured data about that specified system.


You can press the `Esc` key (as many times as required) to return to the main menu.  You can chose the collections option from the main menu.

```bash
:collections
```

From here you can see collections available for use.  This can include collections available in the EE, and also in the local collections directories.  

Select `:13` (cisco.ios) in the collections menu, which will show you the cisco.ios collection modules and plugins.

![ee collections](images/ansible-navigator-collections.png)

You can get information about the components of the collection by selecting a line.  Select `:11` to see the details of the ios_interfaces module.

Press the **Esc** key (3 times) to return to the main menu.  Another way to get information about a module is to use the `:doc` command.  Try using the `:doc` command with the `cisco.ios.facts` module to see detailed documentation for the module.

```bash
:doc cisco.ios.facts
```

We will be using the facts module in our playbook.

### Step 2 - Installing a collection

You can use Ansible modules and playbooks using `ansible` and `ansible-playbook` commands which use collections in the default location on the local host, or using `ansible-navigator` which use collections located in the local project or in an Execution Environment.  Often you will install collections locally in your project when you are developing playbooks, and so we are going to take the opportunity to walk you through installing collections, which is a very simple process.

This workshop environment doesn't yet have all of the collections installed that we need to use `ansible` commands with the `network_cli` connection type specificied in the inventory, so we will start off by installing the required collection.  This will also give us a reason to demonstrate using the `ansible-galaxy` command.

Ansible is configured to enable us to connect and pull collections from Ansible Galaxy, which is the open source upstream repository for collections and roles. 

`ansible-galaxy` will install a collection in the default location.  To determine the default location on your system, you can use the `ansible` command, and look for `ansible collection location`.

```sh
[student@ansible-1 ~]$ ansible --version
ansible [core 2.14.0]
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/student/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3.9/site-packages/ansible
  ansible collection location = /home/student/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.9.13 (main, Nov  9 2022, 13:16:24) [GCC 8.5.0 20210514 (Red Hat 8.5.0-15)] (/usr/bin/python3.9)
  jinja version = 3.1.2
  libyaml = True
```

 Let's go and install a collection using `ansible-galaxy`.

```bash
ansible-galaxy collection install ansible.network
```

You will see the collection install, as well as the collection dependencies.
```
Starting galaxy collection install process
Process install dependency map
Starting collection install process
Downloading https://galaxy.ansible.com/download/ansible-network-2.0.0.tar.gz to /home/student/.ansible/tmp/ansible-local-31103__ps7ezt/tmpepcgux0y/ansible-network-2.0.0-vnl9wdv4
Downloading https://galaxy.ansible.com/download/frr-frr-2.0.2.tar.gz to /home/student/.ansible/tmp/ansible-local-31103__ps7ezt/tmpepcgux0y/frr-frr-2.0.2-thfx9qep
Installing 'ansible.network:2.0.0' to '/home/student/.ansible/collections/ansible_collections/ansible/network'
ansible.network:2.0.0 was installed successfully

...

Installing 'cisco.iosxr:6.0.0' to '/home/student/.ansible/collections/ansible_collections/cisco/iosxr'
Downloading https://galaxy.ansible.com/download/cisco-ios-5.0.0.tar.gz to /home/student/.ansible/tmp/ansible-local-31103__ps7ezt/tmpepcgux0y/cisco-ios-5.0.0-u_poibcq
cisco.iosxr:6.0.0 was installed successfully
Installing 'cisco.ios:5.0.0' to '/home/student/.ansible/collections/ansible_collections/cisco/ios'
cisco.ios:5.0.0 was installed successfully
```

You can use the `ansible-galaxy` command to determine the collections you have installed locally, as well as where they are located.

```bash
[student@ansible-1 ~]$ ansible-galaxy collection list

# /home/student/.ansible/collections/ansible_collections
Collection              Version
----------------------- -------
ansible.netcommon       5.2.0  
ansible.network         2.0.0  
ansible.utils           2.11.0 
arista.eos              6.1.2  
cisco.ios               5.0.0  
cisco.iosxr             6.0.1  
cisco.nxos              5.2.0  
frr.frr                 2.0.2  
junipernetworks.junos   5.3.0  
openvswitch.openvswitch 2.1.1  
vyos.vyos               4.1.0  
```

You can use the `ansible-doc -l` command to list the modules, plugins, filters, etc that you have installed locally, or `ansible-navigator collections` and `ansible-navigator doc` for collections that are installed in your project directory or on your EE.

```bash
ansible-navigator collections
```

If you want to select a line, remember to use `:<line number>`

### Using Collection Content in a Playbook

To use content from a collection in a playbook, provide the fully qualified collection namespace in your playbook.  We will spend a lot more time on this in the following exercises, but as an example:

```yaml
---
- name: Get Cisco network device facts
  hosts: cisco
  gather_facts: no

  tasks:
  - name: Get IOS Facts
    cisco.ios.ios_facts:
  

```

### Using a Role in a Playbook
The classic way to use roles is with the roles option for a given play:

```yaml
---
- name: 
  hosts: all
  roles:
    - network_baseline_role
```

You can also use roles dynamically anywhere in the tasks section of a play using include_role. While roles added in a roles section run before any other tasks in a play, included roles run in the order they are defined. If there are other tasks before an include_role task, the other tasks will run first.

To include a role:
```yaml
---
- name: Use a role in a playbook
  hosts: juniper
  tasks:
    - name: Print a message
      ansible.builtin.debug:
        msg: "this task runs before the example role"

    - name: Include the example role
      include_role:
        name: network_baseline_role

```

## Complete

You have completed lab exercise 2 - Roles and Collections!  

You now understand:

* How to explore **execution environments** with `ansible-navigator`
* How to install collections
* How to see the collections that are installed (on your ee and locally), and the content in them
* How to use collection content in a playbook
* How to use a role in a playbook



---
[Previous Exercise](../1-GettingStarted/README.md)  | [Next Exercise](../3-Inventories/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)
